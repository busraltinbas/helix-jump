using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    public Transform player;
    public GameManager gm;
    public AudioSource drop;
    void Update()
    {
        if (transform.position.y > player.transform.position.y+9)
        {
            Destroy(gameObject);
            drop.Play();

            gm.ScorePlus();
        }
      
    }
    
}
