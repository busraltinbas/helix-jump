using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LastLevelGameManager : MonoBehaviour
{
    public Text scoreText;
    public int score;
    public Slider levelSlider;
    public Transform player;
    public GameObject finishObj;
    float _dist = 0.04f;
 


    public void ScorePlus()
    {
        int currentLevelMinScore = player.GetComponent<FinishLevelPlayerController>()._currentLevelMinScore;

        score++;
        int totalScore = currentLevelMinScore + score;
        scoreText.text = totalScore.ToString();
        levelSlider.value += _dist;
        PlayerPrefs.SetInt("score", score);


    }









}
