using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateCylindir : MonoBehaviour
{
    public int speed;
   
    // Update is called once per frame
    void Update()
    {
        float touchDelta;

        if(Input.touchCount>0 && Input.GetTouch(0).phase==TouchPhase.Moved)
        {
            touchDelta = Input.GetTouch(0).deltaPosition.x / Screen.width;
            transform.Rotate(0, touchDelta * speed * Time.deltaTime, 0);
        }
       

     

        if (Input.GetMouseButton(0))
        {
            touchDelta = Input.GetAxis("Mouse X");
            transform.Rotate(0, touchDelta * speed * Time.deltaTime,0);

        }
        
    }
}
