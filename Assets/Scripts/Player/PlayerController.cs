using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{

    public GameObject levelController;
    public Transform player; 
    private Rigidbody  _rigidbody;
    public int speed;
    public AudioSource jump;
  

    void Start()
    {
        
        _rigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        _rigidbody.velocity += new Vector3(0, 8, 0);
    
        if (collision.gameObject.name == "OtherPlatform")
        {
             levelController.GetComponent<LevelController>().GameOver();
        }
        
        if (collision.gameObject.name == "LastPlatform")
        {
           
            levelController.GetComponent<LevelController>().NextLevel();
        } 
        if (collision.gameObject.name == "Platform")
        {
            jump.Play();
           
        }


    }





    



}
