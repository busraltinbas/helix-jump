using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class FinishLevelPlayerController : MonoBehaviour
{

    public Transform player;
    private Rigidbody _rigidbody;
    public int speed;
    public GameObject gameOver;
    public GameObject gameFinish;
    public AudioSource jump;
    private int currentLevel;
    public int _currentLevelMinScore = 0;
    public LastLevelGameManager gm;
    public Text currentLevelText;
    void Start()
    {
        _currentLevelMinScore = (SceneManager.GetActiveScene().buildIndex - 1) * 21;
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        gm.scoreText.text = 84.ToString();

        currentLevelText.text = (SceneManager.GetActiveScene().buildIndex).ToString();

        _rigidbody = GetComponent<Rigidbody>();
    }


    private void OnCollisionEnter(Collision collision)
    {
        _rigidbody.velocity += new Vector3(0, 8, 0);


        if (collision.gameObject.name == "OtherPlatform")
        {
           
            gameOver.SetActive(true);
            Time.timeScale= 0;

        }

        if (collision.gameObject.name == "LastPlatform")
        {
       
            gameFinish.SetActive(true);
            Time.timeScale = 0;


        }
        if (collision.gameObject.name == "Platform")
        {
            jump.Play();

        }


    }



    public void GameScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
   
    }
    public void GameFinish()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex -5);
        PlayerPrefs.DeleteKey("currentLevel");
    }

}
