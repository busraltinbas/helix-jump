using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public Text scoreText;
    public int score;
    public GameObject levelController;
    public Slider levelSlider;
    public Transform player;
    public GameObject finishObj;
    float _dist=0.04f ;
   


    public void ScorePlus()
    {
        int currentLevelMinScore =levelController.GetComponent<LevelController>()._currentLevelMinScore;
        
        score++;
        int totalScore = currentLevelMinScore + score;
        scoreText.text = totalScore.ToString();
        levelSlider.value +=  _dist;
        PlayerPrefs.SetInt("score", score);
        

    } 


 


    
    
    
   
}
