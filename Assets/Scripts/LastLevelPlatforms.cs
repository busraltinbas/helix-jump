using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastLevelPlatforms : MonoBehaviour
{
    public Transform player;
    public LastLevelGameManager gm;
    public AudioSource drop;
    void Update()
    {
        if (transform.position.y > player.transform.position.y + 9)
        {
            Destroy(gameObject);
            drop.Play();

            gm.ScorePlus();
        }

    }

}
