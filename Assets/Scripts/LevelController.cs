using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelController : MonoBehaviour
{
    public GameManager gm;
    public Text currentLevelText, nextLevelText;
    public GameObject gameOver;
    public GameObject nextLevel;
    private int currentLevel;
    public int _currentLevelMinScore = 0;
   


    private void Start()
    {
        _currentLevelMinScore = (SceneManager.GetActiveScene().buildIndex - 1) * 21;
        currentLevel = PlayerPrefs.GetInt("currentLevel");
        gm.scoreText.text = _currentLevelMinScore.ToString();

        currentLevelText.text = (SceneManager.GetActiveScene().buildIndex).ToString();
        nextLevelText.text = (SceneManager.GetActiveScene().buildIndex + 1).ToString();

        if (SceneManager.GetActiveScene().buildIndex == 6)
        {
            gm.scoreText.text = 84.ToString();
            currentLevelText.text = (SceneManager.GetActiveScene().buildIndex).ToString();
          
        }

    }

    public void GameOver()
    {
        gameOver.SetActive(true);
        Time.timeScale = 0;

    }

    public void LevelUp()
    {
        nextLevel.SetActive(true);
        Time.timeScale = 0;

    }

    public void GameScene()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        PlayerPrefs.SetInt("score", _currentLevelMinScore);
    }
    public void NextLevel()
    {

        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        PlayerPrefs.SetInt("currentLevel", (SceneManager.GetActiveScene().buildIndex + 1));


    }





}
